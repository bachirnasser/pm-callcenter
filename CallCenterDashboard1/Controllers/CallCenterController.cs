﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CallCenterDashboard1.Controllers
{
    public class CallCenterController : Controller
    {
        // GET: CallCenter
        public ActionResult Dashboard()
        {
            return View("Dashboard");
        }


        public ActionResult UserInfo()
        {
            ViewBag.Message = "Your UserInfo page.";

            return View();
        }

        public ActionResult OperationProgress()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult OperationScript()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Clock()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Separator()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult IndividualsList()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Questions()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult FAQs()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult StopWatch()
        {
            ViewBag.Message = "";

            return View();
        }
    }
}